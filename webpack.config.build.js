const Path = require('path');

const Common = require('./webpack.config.common');

/**
 * Config extension
 */

Common.config.entry = {
  'index.html': Common.MAIN_VIEW_PATH,
  'client.js': Common.MAIN_SCRIPT_PATH,
  'styles.css': Common.MAIN_STYLE_PATH
},
Common.config.output = {
  path: Common.OUTPUT_DIR,
  filename: '[name]'
},

module.exports = Common.config;