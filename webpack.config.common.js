const Fs = require('fs');
const Path = require('path');
const Webpack = require('webpack');
const Glob = require('glob');

const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

/**
* 0. Define constants
*/
const VIEWS_DIR = Path.resolve(__dirname, 'Src', 'Views');
const SCRIPTS_DIR = Path.resolve(__dirname, 'Src', 'Scripts');
const TEST_DIR = Path.resolve(__dirname, 'Src', 'Test');
const STYLES_DIR = Path.resolve(__dirname, 'Src', 'Styles');

const MAIN_SCRIPT_PATH = Path.resolve(SCRIPTS_DIR, 'Main.ts');
const MAIN_STYLE_PATH = Path.resolve(STYLES_DIR, 'Main.scss');
const MAIN_VIEW_PATH = Path.resolve(VIEWS_DIR, 'Index.pug');

const OUTPUT_DIR = Path.resolve(__dirname, 'Web');



/**
* 1. Environment setup
*/

const isProduction = process.env.NODE_ENV == 'production' 
|| process.argv.indexOf('-p') != -1;

/**
* 2. Define widget config path, plugins and devtools
*/

const extractScss = new ExtractTextPlugin('[name]')
const extractPug = new ExtractTextPlugin('[name]')
let plugins = [
  extractScss,
  extractPug
];
let devtool = 'inline-source-map';

// overwrite config for production builds
if (isProduction) {
  
  // plugins
  const uglifyJsPlugin = new UglifyJsPlugin({
    'process.env.NODE_ENV': 'production',
    sourceMap: true
  });
  
  plugins.push(uglifyJsPlugin);
  
  // dev tools
  devtool = 'hidden-source-map';
}

/**
* 3. Shared config
*/

const config = {
  target: 'web',
  resolve: {
    extensions: ['.js', '.ts', '.pug', '.scss']
  },
  module: {
    rules: [{
      test: /\.ts$/,
      include: [SCRIPTS_DIR, TEST_DIR],
      use: ['ts-loader']
    }, {
      test: /\.pug$/,
      include: [VIEWS_DIR],
      use: extractPug.extract({ use: ['html-loader', 'pug-html-loader'] })
    }, {
      test: /\.scss$/,
      include: [STYLES_DIR],
      use: extractScss.extract({ use: ['css-loader', 'sass-loader'] })
    }]
  },
  devtool: devtool,
  plugins: plugins
}

module.exports = {
  SCRIPTS_DIR,
  STYLES_DIR,
  VIEWS_DIR,

  MAIN_SCRIPT_PATH,
  MAIN_STYLE_PATH,
  MAIN_VIEW_PATH,

  OUTPUT_DIR,
  
  config
};