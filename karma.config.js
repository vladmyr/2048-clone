const WebpackCommon = require('./webpack.config.common');

module.exports = function(config) {
  config.set({
    basePath: './',
    frameworks: ['mocha', 'chai'],
    files: [
      'Src/Scripts/Test/**/*.ts'
    ],
    preprocessors: {
      'Src/Scripts/Test/*.ts': [
        'webpack',
        'sourcemap'
      ]
    },
    webpack: {
      devtool: 'eval',
      module: WebpackCommon.config.module,
      resolve: WebpackCommon.config.resolve
    },
    mime: {
      'text/x-typescript': ['ts','tsx']
    },
    plugins: [
      'karma-*'
    ],
    reporters: ['progress'],
    port: 8765,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['ChromeHeadless'],
    singleRun: false,
    concurrency: Infinity
  })
}