const config = require('./webpack.config.build');

/**
 * Config extension
 */

config.watch = true
config.watchOptions = {
    aggregateTimeout: 500
}

module.exports = config;