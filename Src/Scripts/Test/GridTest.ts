import { assert } from 'chai';

import Tile from '../Tile';
import Slot from '../Slot';
import Grid from '../Grid';

class GridTest extends Grid {
  public static ElGrid = document.createElement('div');

  public static SerializeVector(vector: Slot[] = []) {
    return vector.map((slot) => {
      return slot.getTile().getValue()
    }).join('');
  }
}

describe ('Grid', () => {
  const grid = new GridTest(4);

  describe ('#calcMoveResult', () => {
    it ('0002 -> 2000', () => {
      const vector = [
        new Slot(0, new Tile(0)),
        new Slot(1, new Tile(0)),
        new Slot(2, new Tile(0)),
        new Slot(3, new Tile(2))
      ]

      grid.calcMoveResult(vector);

      assert.equal(GridTest.SerializeVector(vector), '2000');
    });

    it ('0022 -> 4000', () => {
      const vector = [
        new Slot(0, new Tile(0)),
        new Slot(1, new Tile(0)),
        new Slot(2, new Tile(2)),
        new Slot(3, new Tile(2))
      ]

      grid.calcMoveResult(vector);

      assert.equal(GridTest.SerializeVector(vector), '4000');
    });

    it ('2200 -> 4000', () => {
      const vector = [
        new Slot(0, new Tile(2)),
        new Slot(1, new Tile(2)),
        new Slot(2, new Tile(0)),
        new Slot(3, new Tile(0))
      ]

      grid.calcMoveResult(vector);

      assert.equal(GridTest.SerializeVector(vector), '4000');
    });

    it ('0202 -> 4000', () => {
      const vector = [
        new Slot(0, new Tile(0)),
        new Slot(1, new Tile(2)),
        new Slot(2, new Tile(0)),
        new Slot(3, new Tile(2))
      ]

      grid.calcMoveResult(vector);

      assert.equal(GridTest.SerializeVector(vector), '4000');
    });

    it ('2002 -> 4000', () => {
      const vector = [
        new Slot(0, new Tile(2)),
        new Slot(1, new Tile(0)),
        new Slot(2, new Tile(0)),
        new Slot(3, new Tile(2))
      ]

      grid.calcMoveResult(vector);

      assert.equal(GridTest.SerializeVector(vector), '4000');
    });

    it ('0222 -> 4200', () => {
      const vector = [
        new Slot(0, new Tile(0)),
        new Slot(1, new Tile(2)),
        new Slot(2, new Tile(2)),
        new Slot(3, new Tile(2))
      ]

      grid.calcMoveResult(vector);

      assert.equal(GridTest.SerializeVector(vector), '4200');
    });

    it ('2222 -> 4400', () => {
      const vector = [
        new Slot(0, new Tile(2)),
        new Slot(1, new Tile(2)),
        new Slot(2, new Tile(2)),
        new Slot(3, new Tile(2))
      ]

      grid.calcMoveResult(vector);

      assert.equal(GridTest.SerializeVector(vector), '4400');
    });

    it ('2044 -> 2800', () => {
      const vector = [
        new Slot(0, new Tile(2)),
        new Slot(1, new Tile(0)),
        new Slot(2, new Tile(4)),
        new Slot(3, new Tile(4))
      ]

      grid.calcMoveResult(vector);

      assert.equal(GridTest.SerializeVector(vector), '2800');
    });

    it ('2404 -> 2800', () => {
      const vector = [
        new Slot(0, new Tile(2)),
        new Slot(1, new Tile(4)),
        new Slot(2, new Tile(0)),
        new Slot(3, new Tile(4))
      ]

      grid.calcMoveResult(vector);

      assert.equal(GridTest.SerializeVector(vector), '2800');
    });

    it ('2244 -> 4800', () => {
      const vector = [
        new Slot(0, new Tile(2)),
        new Slot(1, new Tile(2)),
        new Slot(2, new Tile(4)),
        new Slot(3, new Tile(4))
      ]

      grid.calcMoveResult(vector);

      assert.equal(GridTest.SerializeVector(vector), '4800');
    });

    it ('00016 -> 16000', () => {
      const vector = [
        new Slot(0, new Tile(0)),
        new Slot(1, new Tile(0)),
        new Slot(2, new Tile(0)),
        new Slot(3, new Tile(16))
      ]

      grid.calcMoveResult(vector);

      assert.equal(GridTest.SerializeVector(vector), '16000');
    });

    it ('8200 -> 8200', () => {
      const vector = [
        new Slot(0, new Tile(8)),
        new Slot(1, new Tile(2)),
        new Slot(2, new Tile(0)),
        new Slot(3, new Tile(0))
      ]

      grid.calcMoveResult(vector);

      assert.equal(GridTest.SerializeVector(vector), '8200');
    });

    it ('81680 -> 81680', () => {
      const vector = [
        new Slot(0, new Tile(8)),
        new Slot(1, new Tile(16)),
        new Slot(2, new Tile(8)),
        new Slot(3, new Tile(0))
      ]

      grid.calcMoveResult(vector);

      assert.equal(GridTest.SerializeVector(vector), '81680');
    });

    it ('08168 -> 81680', () => {
      const vector = [
        new Slot(0, new Tile(0)),
        new Slot(1, new Tile(8)),
        new Slot(2, new Tile(16)),
        new Slot(3, new Tile(8))
      ]

      grid.calcMoveResult(vector);

      assert.equal(GridTest.SerializeVector(vector), '81680');
    });

    it ('2004 -> 2400', () => {
      const vector = [
        new Slot(0, new Tile(2)),
        new Slot(1, new Tile(0)),
        new Slot(2, new Tile(0)),
        new Slot(3, new Tile(4))
      ]

      grid.calcMoveResult(vector);

      assert.equal(GridTest.SerializeVector(vector), '2400');
    });

    it ('0161616 -> 321600', () => {
      const vector = [
        new Slot(0, new Tile(0)),
        new Slot(1, new Tile(16)),
        new Slot(2, new Tile(16)),
        new Slot(3, new Tile(16))
      ]

      grid.calcMoveResult(vector);

      assert.equal(GridTest.SerializeVector(vector), '321600');
    });
  });
});