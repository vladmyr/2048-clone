import Tile from './Tile';

class Slot {
  public static GridSize = 0;
  public static ElGrid: HTMLElement = undefined;

  private _tile: Tile = undefined;
  private _idx: number = -1;
  private _element: HTMLElement = undefined;

  public constructor(idx: number, tile: Tile = undefined) {
    this._idx = idx;
    this._tile = tile || new Tile();
    this._createElement();
  }

  public getIdx() { return this._idx; }
  public getTile() { return this._tile; }
  public viewGetClientWidth() { return this._element.clientWidth; }
  public viewGetClientHeight() { return this._element.clientHeight; }

  public getElement() { return this._element; }

  private _createElement() {
    if (this._element) {
      return;
    }

    this._element = document.createElement('div');
    this._element.classList.add('slot');
    this._element.style.width = `${Math.floor(100 / Slot.GridSize)}%`;

    // insert tile
    this._element.insertBefore(this._tile.getElement(), null);

    // insert slot
    Slot.ElGrid.insertBefore(this._element, null);

    return;
  }
}

export default Slot;