class Score {
  private _element: HTMLElement;
  private _value: number = 0;

  public constructor(value: number = 0) {
    this._value = value;
    this._element = document.getElementById('score');
  }

  public add(value: number = 0) {
    this._value += value;
    return;
  }

  public reset() {
    this._value = 0;
    return;
  }

  public viewRender() {
    if (this._element) this._element.innerHTML = this._value.toString();
    return;
  }
}

export default Score;