import { MOVE } from './Tile';
import Controls from './Controls';
import Grid from './Grid';

class Main {
  private _controls: Controls;
  private _grid: Grid;

  private _controlsEventListenerBound;

  public constructor() {
    this._controls = new Controls();
    this._grid = new Grid(4);

    this._controlsEventListenerBound = this._controlsEventlistener.bind(this);

    // init controls event listerns
    this._controls.registerListener(MOVE.TOP, this._controlsEventListenerBound);
    this._controls.registerListener(MOVE.BOTTOM, this._controlsEventListenerBound);
    this._controls.registerListener(MOVE.LEFT, this._controlsEventListenerBound);
    this._controls.registerListener(MOVE.RIGHT, this._controlsEventListenerBound);
    this._controls.registerListener(MOVE.RESET, this._controlsEventListenerBound);

    // init grid
    this._grid.init();
  }

  private _controlsEventlistener(move: number) {
    if (move == MOVE.RESET){
      this._grid.reset();
    } else {
      this._grid.move(move);
    }
    
    return;
  }
}

const main = new Main();