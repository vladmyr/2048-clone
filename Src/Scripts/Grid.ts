import Slot from './Slot';
import Tile, { MOVE, ILocation2d } from './Tile';
import Score from './Score';

interface IMoveResultIteratee {
  (slot: Slot, targetSlot: Slot): void
}

enum PLAY_STATE {
  ONGOIGN,
  WIN,
  LOST
}

class Grid {
  private static readonly _WIN_VALUE = 2048;

  public static ElGrid: HTMLElement = document.querySelector('#grid') as HTMLElement;

  private _isLocked: boolean = false;
  private _hasUpdatedOnLastMove: boolean = false;
  private _gridSize: number = 4;
  private _matrix: Slot[] = [];
  private _idxSlotsEmpty: Set<number> = new Set();
  private _idxSlotsOccupied: Set<number> = new Set();
  private _score: Score;

  private static _ToArray<T>(iterator: Iterator<T>) {
    const arr = [];
    let next: IteratorResult<T> = iterator.next();
    
    do {
      if (typeof next.value != 'undefined') arr.push(next.value);
      next = iterator.next();
    } while (!next.done);

    return arr;
  }

  public constructor(gridSize: number) {
    this._gridSize = gridSize;

    Slot.ElGrid = this.constructor.ElGrid;
    Slot.GridSize = this._gridSize;

    this._score = new Score();
  }

  public init() {
    this._matrix.length = this._gridSize * this._gridSize;

    // initialize slots
    this._matrix = this._matrix
      .fill(undefined)
      .map((value, idx) => {
        this._idxSlotsEmpty.add(idx);

        return new Slot(idx);
      });

    this.generateTile();
    this.generateTile();
  }

  public async move(move: number): Promise<void> {
    if (this._isLocked) return;

    const concurrentExecution = [];
    
    this._hasUpdatedOnLastMove = false;
    
    // apply move to each vector
    for (let idx = 0, l = this._matrix.length; idx < l; idx += this._gridSize + 1) {
      // execute concurrelty
      concurrentExecution.push(this.moveVector.call(this, idx, move));
    }

    // wait untill rendering completes
    await Promise.all(concurrentExecution);

    this._updatePlayState();

    // generate new tile if matrix was updated
    if (this._hasUpdatedOnLastMove) this.generateTile();
    
    this._Print();

    return;
  }

  public reset() {

    // reset score
    this._score.reset();
    this._score.viewRender();

    // reset matrix
    this._matrix
      .forEach((slot, idx) => {
        this._idxSlotsEmpty.add(idx);
        this._idxSlotsOccupied.delete(idx);

        slot.getTile().destructor();
      });

    // reset state
    this._updatePlayState();

    // place tiles
    this.generateTile();
    this.generateTile();

    return;
  }

  protected async moveVector(idx: number, move: number): Promise<void> {
    const vectorIndices = this.getMoveVectorIndices(idx, move);
    const vector = this.getMoveVector(idx, move);

    let hasIterateeFired = false;

    return new Promise<void>((fulfill) => {
      this.calcMoveResult(vector, async (slot, targetSlot) => {
        hasIterateeFired = true;
        this._hasUpdatedOnLastMove = true;
        await this.moveIteratee(slot, targetSlot, move);
        return fulfill();
      });

      if (!hasIterateeFired) {
        return fulfill();
      }
    })
  }

  protected async moveIteratee(
    slot: Slot, 
    targetSlot: Slot, 
    move: number
  ): Promise<void> {
    // if source is the same as target, skip execution
    if (slot == targetSlot)
      return;

    const tile = slot.getTile();
    const targetTile = targetSlot.getTile();

    // update indices
    this._idxSlotsOccupied.add(targetSlot.getIdx());
    this._idxSlotsOccupied.delete(slot.getIdx());
    this._idxSlotsEmpty.add(slot.getIdx());
    this._idxSlotsEmpty.delete(targetSlot.getIdx());

    // render related processing
    const location2d = this.viewCalcLocation2d(slot, targetSlot, move);
    tile.setLocation2d(location2d.x, location2d.y);

    await tile.viewRenderMove();
    if (tile.getValue()) {
      tile.renderReset();
    } else {
      tile.destructor();
    }

    targetTile.render();

    if (targetTile.getHasValueDoubled()) {
      targetTile.renderPulse();

      this._score.add(targetTile.getValue());
      this._score.viewRender();
    }

    return;
  }

  protected generateTile() {
    if (this._isLocked) return;

    const size = this._idxSlotsEmpty.size;

    if (!size) return;

    const slots = Grid._ToArray(this._idxSlotsEmpty.values());
    const randomIdx = Math.round(Math.random() * (size - 1));
    const slotIdx = slots[randomIdx];

    // place tile on matrix
    this._matrix[slotIdx].getTile().generateValue();
    this._matrix[slotIdx].getTile().render();

    // update indexes
    this._idxSlotsEmpty.delete(slotIdx);
    this._idxSlotsOccupied.add(slotIdx);

    return;
  }

  public calcMoveResult(
    vector: Slot[], 
    iteratee: IMoveResultIteratee = undefined
  ): Slot[] {
    for (let i = 0, l = this._gridSize - 1; i < l; i++) {
      const slot = vector[i];

      let j = i + 1;
      let isPairFound = false;

      while(!isPairFound && j < this._gridSize) {
        const nextSlot = vector[j];

        // tile is not emply
        if (slot.getTile().getValue()) {
          if (nextSlot.getTile().getValue() == slot.getTile().getValue()) {
            slot.getTile().doubleValue();
            nextSlot.getTile().clearValue();
            isPairFound = true;

            if (iteratee) iteratee(nextSlot, slot);
          } else if (nextSlot.getTile().getValue()) {
            isPairFound = true;
          }
        } else if (nextSlot.getTile().getValue()) {
          slot.getTile().setValue(nextSlot.getTile().getValue());
          nextSlot.getTile().clearValue();

          if (iteratee) iteratee(nextSlot, slot);
        }

        j++;
      }
    }

    return vector;
  }

  protected getMoveVector(idx: number, move: number): Slot[] {
    const vectorIndices = this.getMoveVectorIndices(idx, move);
    const vector = vectorIndices.map(idx => this._matrix[idx]);

    return vector;
  }

  protected getMoveVectorIndices(idx: number, move: number): number[] {
    const length = this._matrix.length;
    const column = idx % this._gridSize;
    const row = Math.floor(idx / this._gridSize);

    const vector: number[] = [];

    // get initial index
    switch (move) {
      case (MOVE.TOP): 
        idx = column; 
        break;
      case (MOVE.BOTTOM): 
        idx = length - (this._gridSize - column); 
        break;
      case (MOVE.LEFT): 
        idx = row * this._gridSize; 
        break;
      case (MOVE.RIGHT): 
        idx = row * this._gridSize + (this._gridSize - 1); 
        break;
    }

    for (let i = 0; i < this._gridSize; i++) {
      vector.push(idx);

      // calc next index
      switch (move) {
        case (MOVE.TOP): 
          idx += this._gridSize;
          break;
        case (MOVE.BOTTOM): 
          idx -= this._gridSize;
          break;
        case (MOVE.LEFT): 
          idx++;
          break;
        case (MOVE.RIGHT): 
          idx--;
          break;
      }
    }

    return vector;
  }

  protected viewCalcLocation2d(slot: Slot, targetSlot: Slot, move: number): ILocation2d {
    const vectorIndices = this.getMoveVectorIndices(slot.getIdx(), move);
    const idxDiff = Math.abs(vectorIndices.indexOf(slot.getIdx()) - vectorIndices.indexOf(targetSlot.getIdx()));
    const clientSlotSize = slot.viewGetClientWidth();
    const location2d: ILocation2d = {
      x: 0,
      y: 0
    }

    switch (move) {
      case MOVE.TOP:
        location2d.y = idxDiff * clientSlotSize * -1;
        break;
      case MOVE.BOTTOM:
        location2d.y = idxDiff * clientSlotSize;
        break;
      case MOVE.LEFT:
        location2d.x = idxDiff * clientSlotSize * -1;
        break;
      case MOVE.RIGHT:
        location2d.x = idxDiff * clientSlotSize;
        break;
    }

    return location2d;
  }

  private _viewRenderPlayState(state: number) {
    switch (state) {
      case PLAY_STATE.ONGOIGN:
        this._isLocked = false;
        Grid.ElGrid.classList.remove('has-won');
        Grid.ElGrid.classList.remove('has-lost');
        break;
      case PLAY_STATE.WIN:
        this._isLocked = true;
        Grid.ElGrid.classList.remove('has-lost');
        Grid.ElGrid.classList.add('has-won');
        break;
      case PLAY_STATE.LOST:
        this._isLocked = true;
        Grid.ElGrid.classList.remove('has-won');
        Grid.ElGrid.classList.add('has-lost');
        break;
    }

    return;
  }

  private _updatePlayState() {
    const slotOccupiedIndices = Grid._ToArray(this._idxSlotsOccupied.values());
    
    let hasWon = false;
    let i = 0;

    while (!hasWon && i < slotOccupiedIndices.length) {
      const slot = this._matrix[slotOccupiedIndices[i]];
      hasWon = slot.getTile().getValue() == Grid._WIN_VALUE;
      i++;
    }
    
    if (hasWon) {
      this._isLocked = true;
      this._viewRenderPlayState(PLAY_STATE.WIN);
    } else if (!this._idxSlotsEmpty.size) {
      this._isLocked = true;
      this._viewRenderPlayState(PLAY_STATE.LOST);
    } else if (this._isLocked) {
      this._isLocked = false;
      this._viewRenderPlayState(PLAY_STATE.ONGOIGN);
    }

    return;
  }

  private _Print() {
    const formattedValues = [];

    for (let i = 0; i < this._matrix.length; i++) {
      const value = this._matrix[i].getTile().getValue().toString();
      formattedValues.push('     '.substring(0, 5 - value.length) + value);

      if ((i + 1) % this._gridSize == 0) {
        formattedValues.push('\n');
      }
    }
    
    console.log(formattedValues.join(''));
  }

  // NOTE: ts 2.5 does not have strongly typed constructors and
  // this workaround allows to call static properties of a class.
  // Reference: https://github.com/Microsoft/TypeScript/issues/3841#issuecomment-298199835
  'constructor': typeof Grid;
}

export default Grid;