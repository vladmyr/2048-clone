export interface ILocation2d {
  x: number,
  y: number
}

export enum MOVE {
  TOP,
  BOTTOM,
  LEFT,
  RIGHT,
  RESET
}

class Tile {
  private static readonly INITIAL_VALUE_CANDIDATES = [2, 4];
  private static readonly TIMING = {
    MOVE: 100,
    PULSE: 100,
    DROP_IN: 2000,
  }
  private static readonly EVENT = {
    TRANSITION_END: 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend'
  }

  private _value: number = 0;
  private _hasValueDoubled: boolean = false;
  private _element: HTMLElement = undefined;
  private _location2d = {
    x: 0,
    y: 0
  }

  public constructor(value: number = 0) {
    this._value = value;
    this._createElement();
  }

  public generateValue() {
    this._value = Tile.INITIAL_VALUE_CANDIDATES[Math.round(Math.random())];
  }

  public getHasValueDoubled() { return this._hasValueDoubled; }

  public getElement() { return this._element; }
  public getValue() { return this._value; }
  public setValue(value: number) { 
    this._value = value; 
    this._hasValueDoubled = false;
  }

  public clearValue() { this.setValue(0); }
  public doubleValue() { 
    this._value *= 2; 
    this._hasValueDoubled = true;
    return;
  }

  public setLocation2d(x: number, y: number) {
    this._location2d.x = x;
    this._location2d.y = y;

    return;
  }

  public destructor() {
    this._value = 0;
    this._hasValueDoubled = false;
    this._location2d.x = 0;
    this._location2d.y = 0;
    this.render();

    return;
  }

  public async viewRenderMove(): Promise<void> {
    this._setTransitionDuration(Tile.TIMING.MOVE);
    this._setTranslate2d(this._location2d.x, this._location2d.y);

    await new Promise((fulfill) => {
      return setTimeout(fulfill, Tile.TIMING.MOVE);
    });

    this._setTransitionDuration(0);

    return;
  }

  public render() {
    const strValue = this._value.toString();

    this._element.setAttribute('data-value', strValue);
    this._element.innerText = strValue;
    this._element.style.transform = '';

    if (this._value) {
      this._element.classList.add('is-visible');
    } else {
      this._element.classList.remove('is-visible');
    }

    return;
  }

  public async renderPulse() {
    this._setAnimationDuration(Tile.TIMING.PULSE);
    this._element.classList.add('pulse');

    await new Promise((fulfill) => {
      return setTimeout(fulfill, Tile.TIMING.PULSE);
    });

    this._setAnimationDuration(0);
    this._element.classList.remove('pulse');

    return;
  }

  public renderReset() {
    this._hasValueDoubled = false;
    this._location2d.x = 0;
    this._location2d.y = 0;
    this._setTranslate2d();
    return;
  }

  private _createElement() {
    if (this._element) {
      return;
    }

    this._element = document.createElement('div');
    this._element.classList.add('tile');

    return;
  }

  private _setTranslate2d(x: number = 0, y: number = 0) {
    if (x || y) {
      this._element
        .style
        .transform = `translate3d(${x}px, ${y}px, 0)`;
    } else {
      this._element.style.transform = '';
    }

    return;
  }

  private _setTransitionDuration(ms: number) {
    if (ms) {
      this._element.style.transitionDuration = `${ms / 1000}s`;
    } else {
      this._element.style.transitionDuration = '';
    }

    return;
  }

  private _setAnimationDuration(ms: number) {
    if (ms) {
      this._element.style.animationDuration = `${ms / 1000}s`;
    } else {
      this._element.style.animationDuration = '';
    }
  }
}

export default Tile;