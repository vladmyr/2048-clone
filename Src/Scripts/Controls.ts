import { MOVE } from './Tile';

interface IListener {
  (move: number): void
}

interface IListeners {
  [key: number]: IListener[]
}

enum KEY {
  LEFT = 37,
  UP = 38,
  RIGHT = 39,
  DOWN = 40,
  R = 82
}

class Controls {
  private static readonly KEY_EVENT = 'keydown';
  private static readonly TOUCH_EVENT = {
    START: 'touchstart',
    END: 'touchend',
    MOVE: 'touchmove',

    MS: {
      START: 'MSPoinerDown',
      END: 'MSPointerMove',
      MOVE: 'MSPointerUp',
    }
  }

  private _keyboardEventBound;
  private _listeners: IListeners = {};

  private _elementNewGame: HTMLElement; 

  public constructor() {
    this._initKeyboardControls();
    this._initGestureControls();
    this._initViewControls();
  }

  public registerListener(move: number, listener) {
    if (!this._listeners[move]) this._listeners[move] = [];
    this._listeners[move].push(listener);
    return;
  }

  private _initViewControls() {
    this._elementNewGame = document.getElementById('btn-new-game');
    this._elementNewGame.addEventListener('click', this._callListeners.bind(this, MOVE.RESET));
    return;
  }

  private _initKeyboardControls() {
    this._keyboardEventBound = this._keyboardEvent.bind(this);

    document.addEventListener(Controls.KEY_EVENT, this._keyboardEventBound);
    return;
  }

  private _initGestureControls() {
    const element = document.getElementById('grid');

    if (!element) return;

    const touchEvents = window.navigator.msPointerEnabled
      ? Controls.TOUCH_EVENT.MS
      : Controls.TOUCH_EVENT

    let startX = 0;
    let startY = 0;

    element.addEventListener(touchEvents.START, (event: TouchEvent) => {
      if (window.navigator.msPointerEnabled) {
        startX = (event as any).pageX;
        startY = (event as any).pageY;
      } else {
        startX = event.touches[0].clientX;
        startY = event.touches[0].clientY;
      }

      event.preventDefault();
      return;
    });

    element.addEventListener(touchEvents.MOVE, (event) => {
      event.preventDefault();
      return;
    });

    element.addEventListener(touchEvents.END, (event: TouchEvent) => {
      let endX = 0;
      let endY = 0;

      if (window.navigator.msPointerEnabled) {
        endX = (event as any).pageX;
        endY = (event as any).pageY;
      } else {
        endX = event.changedTouches[0].clientX;
        endY = event.changedTouches[0].clientY;
      }

      const diffX = startX - endX;
      const diffY = startY - endY;

      const absDiffX = Math.abs(diffX);
      const absDiffY = Math.abs(diffY);

      if (Math.max(absDiffX, absDiffY) > 10) {
        this._callListeners(absDiffX > absDiffY
          ? diffX > 0 ? MOVE.LEFT : MOVE.RIGHT
          : diffY > 0 ? MOVE.TOP : MOVE.BOTTOM
        );
      }

      return;
    });

    return;
  }

  private _keyboardEvent(event: KeyboardEvent) {
    const hasModifier = event.altKey || event.ctrlKey || event.metaKey || event.shiftKey

    // if modifier is pressed, exit
    if (hasModifier) 
      return;

    switch (event.which) {
      case KEY.DOWN:
        this._callListeners(MOVE.BOTTOM);
        break;
      case KEY.UP:
        this._callListeners(MOVE.TOP);
        break;
      case KEY.LEFT:
        this._callListeners(MOVE.LEFT);
        break;
      case KEY.RIGHT:
        this._callListeners(MOVE.RIGHT);
        break;
      case KEY.R:
        this._callListeners(MOVE.RESET);
        break;
    }

    return;
  }

  private _callListeners(move: number) {
    if (this._listeners[move]) {
      this._listeners[move].forEach((listener) => {
        listener(move);
        return;
      });
    }

    return;
  }
}

export default Controls;