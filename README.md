# 2048 Clone

A clone of a popular game 2048. 

## I want to play!

Just [click here](https://vladmyr.gitlab.io/2048-clone-pages/) and have fun :)

## Building and running project locally

The only prerequisit is that host machine should have node (ideally v8.x), npm and yarn.
After cloning project execute the following command:

```bash
npm run dev:build
```

Afterwards, open file `Web/index.html` in your favourite browser.

## Project architecture

### Technological stack

- Frontend
  - Typescript
  - Node-sass - SASS preprocessor
  - Pugjs - templating engine for static html
- Build systems
  - yarn
  - webpack

### Why TypeScript?

TypeScript is JS preprocessor language that brings typing features. The biggest advantage of using it is a compilation time static analysis that simplifies code debugging. Ability to define types and interfaces greatly helps keeping data structure of a codebase under control. These key features allow to scale application quicker and easier compared to plain JS.


### Why SASS?

Simply because it is much richer compared to plain CSS. Yet it has syntax a 'CSS-only' person can read and understand easily

### Wht webpack?

Webpack provides a wide randge of configuration possibilities. System of loaders and source file transformation opens a possibility to tweak the building process to very specific requirements. Besides it plays very nice with React ecosystem (React, Redux, Hot Module Reload, etc.). Sometimes It is hard to setup but it pays back once it is done.

### Project structure

Structure is pretty self expanatory. Schematically it looks like this:
```
├── Src
│   ├── Scripts
│   │   └── Test
│   │   │   └── ...
│   │   └── ...
│   ├── Styles
│   │   └── ...
│   ├── Views
│   │   └── index.pug
├── Web
│   └── ...
└── README.md
└── webpack.config.build.js
└── karma.config.json
└── yarn.lock
└── ...
```
- `Web` contains the output of build process execution
- `Src/Script/Test` contains unit test 

### A word about the codebase architecture
The general goal was to replicate original game as close as possible. At the same time code should be generic, reusable and extendable. With that in mind, some HTML and CSS responsibilities were put on JavaScript, like creating tiles, handling transitions/animation, etc. Main benefits that were achieved in result are:

- Full animation flow and sequences control from JS
- Much better ui responsiveness
- Ability to specify an arbitrary grid size

Doing best to follow DRY and single responsibility prinsiple I ended up having such list of building blocks:

- `Tile` - a grid tile attached to slot
- `Slot` - a slot of a grid
- `Score` - a calculate and dislay game score
- `Grid` - incapsulates all above, controls animation flow and sequences, dispatches events
- `Controls` - handles user input from keyboard and touch gestures
- `Main` - to rule them all

### Known issues
- An inappropriate animation for newly generated tile might occurr which is a  transitioning into opposite direction after move action was performed.

## Running test

Project is supplied with minial set of unit tests for core functionality. To run then execute:

```bash
npm test
```

## Changelog

### 1.0.1 - better mobile experience
  - Fix mobile scrolling

### 1.0.0 - initial releast
  - Create of a grid that is randomly populated with two tiles
  - Move the tiles when user presses arrow keys
  - Generate new tiles for every turn
  - Merge two colliding tiles into one
  - Apply the same color to tiles of same value
  - Display a message when the game is "won" or when no more moves can be made
  - Add transitions when tiles are moved and merged
  - Add touch/mobile support – swipe to move the tiles